package br.com.algartelecom.service;

import br.com.algartelecom.model.DumpSubscriber;
import br.com.algartelecom.repositories.DumpSubscriberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

@Service
public class MilhasSubscriberService {

//    private static final String CN = "17";
//    private static final String PREFIXO = "99998";
//    private static final Integer CT_NUM_CD_START = 0;
//    private static final Integer CT_NUM_CD_END = 2999;

    @Autowired
    DumpSubscriberRepository dumpSubscriberRepository;

    public void saveMilhas() {

        Scanner scanner = null;
        try {
            scanner = new Scanner(new File("/home/ericson/Documentos/INFORMACOES/Informações/Projeto SMSC/dump_milhas.txt"));

            /****
             * Formato do arquivo
             *
             * 17,99998,0,2999
             * 34,99854,0,4999
             * 64,99669,0,2999
             * 67,99899,0,4999
             * 35,99674,0,4999
             * 37,99666,0,2999
             *
             * */

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        while (scanner.hasNext()) {
            String linha = scanner.nextLine();
            Scanner linhaScanner = new Scanner(linha);
            linhaScanner.useLocale(Locale.US);
            linhaScanner.useDelimiter(",");

            String cn = linhaScanner.next();
            String prefixo = linhaScanner.next();
            Integer ctNumCdStart = linhaScanner.nextInt();
            Integer ctNumCdEnd = linhaScanner.nextInt();

            for (int x = ctNumCdStart; x <= ctNumCdEnd; x++) {
                DumpSubscriber dumpSubscriber = new DumpSubscriber();
                dumpSubscriber.setMsisdn("55" + cn + prefixo + String.format("%04d", x));
                dumpSubscriber.setFlag(false);
                dumpSubscriberRepository.save(dumpSubscriber);
            }

        }

        scanner.close();

    }

    public List<DumpSubscriber> getAll() {
        return dumpSubscriberRepository.findAll();
    }

    public List<DumpSubscriber> getMsisdns() {
        return dumpSubscriberRepository.getMsisdns();
    }

    public ResponseEntity<DumpSubscriber> changeFlagToOne(String msisdn) {
        DumpSubscriber dumpSubscriber = dumpSubscriberRepository.findByMsisdn(msisdn);

        dumpSubscriber.setFlag(true);
        dumpSubscriberRepository.save(dumpSubscriber);
        return ResponseEntity.ok(dumpSubscriber);
    }

    public void deleteMsisdns() {
       dumpSubscriberRepository.deleteAll();
    }
}
