package br.com.algartelecom.service;

import br.com.algartelecom.model.DumpSubscriber;
import br.com.algartelecom.repositories.DumpSubscriberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

@Service
public class DumpSubscriberService {

    @Autowired
    DumpSubscriberRepository dumpSubscriberRepository;

    public void saveDump() {
        Scanner scanner = null;
        try {
            scanner = new Scanner(new File("/opt/planeta/subscriber_prod.txt"));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        while (scanner.hasNext()) {
            String linha = scanner.nextLine();
            Scanner linhaScanner = new Scanner(linha);
            linhaScanner.useLocale(Locale.US);
            linhaScanner.useDelimiter(",");

            String msisdn = linhaScanner.next();

            if(msisdn.contains("#")){
                msisdn =  msisdn.substring(msisdn.indexOf("#") + 1);
            }

            if (!msisdn.isEmpty()) {
                DumpSubscriber dumpSubscriber = new DumpSubscriber();
                dumpSubscriber.setMsisdn(msisdn);
                dumpSubscriber.setFlag(false);
                dumpSubscriberRepository.save(dumpSubscriber);
            }
        }
        scanner.close();
    }

    public List<DumpSubscriber> getAll() {
        return dumpSubscriberRepository.findAll();
    }

    public List<DumpSubscriber> getMsisdns() {
        return dumpSubscriberRepository.getMsisdns();
    }

    public ResponseEntity<DumpSubscriber> changeFlagToOne(String msisdn) {
        DumpSubscriber dumpSubscriber = dumpSubscriberRepository.findByMsisdn(msisdn);

        dumpSubscriber.setFlag(true);
        dumpSubscriberRepository.save(dumpSubscriber);
        return ResponseEntity.ok(dumpSubscriber);
    }

    public void deleteMsisdns() {
       dumpSubscriberRepository.deleteAll();
    }
}
