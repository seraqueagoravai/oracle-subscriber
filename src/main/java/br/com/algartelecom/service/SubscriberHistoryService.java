package br.com.algartelecom.service;

import br.com.algartelecom.model.LogAction;
import br.com.algartelecom.model.Subscriber;
import br.com.algartelecom.repositories.SubscriberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubscriberHistoryService {

    @Autowired
    SubscriberRepository subscriberRepository;

    @Autowired
    SubscriberService subscriberService;

    public void createHistorys() {
        List<Subscriber> subscriberList= subscriberRepository.getSubscriberWithoutHistory();

        subscriberList.forEach(subscriber -> {
            if(subscriber != null){
                subscriberService.addSubscriberHistoryEntry(subscriber.getCreationUser(), subscriber.getCreationDate(), LogAction.SAVE, subscriber);
            }
        });
    }

}
