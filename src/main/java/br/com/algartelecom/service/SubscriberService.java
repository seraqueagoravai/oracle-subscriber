package br.com.algartelecom.service;

import br.com.algartelecom.model.*;
import br.com.algartelecom.repositories.DumpSubscriberRepository;
import br.com.algartelecom.repositories.SubscriberHistoryRepository;
import br.com.algartelecom.repositories.SubscriberRepository;
import br.com.algartelecom.repositories.SubscriberTypeRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.List;

@Service
public class SubscriberService {

    private static final Logger logger = LoggerFactory.getLogger(SubscriberService.class);

    ObjectMapper mapper = new ObjectMapper();

    @Autowired
    SubscriberRepository subscriberRepository;

    @Autowired
    SubscriberTypeRepository subscriberTypeRepository;

    @Autowired
    DumpSubscriberRepository dumpSubscriberRepository;

    @Autowired
    SubscriberHistoryRepository subscriberHistoryRepository;

    @Autowired
    RestTemplate restTemplate;

//    @Scheduled(fixedDelay = 1000)
    public void saveSubscriber() throws Exception {

        List<SubscriberType> subscriberTypeList = subscriberTypeRepository.findAll();
        List<DumpSubscriber> dumpSubscriberList = dumpSubscriberRepository.getMsisdns();
        logger.info("Results {}", dumpSubscriberList);
        for (DumpSubscriber dumpSubscriber : dumpSubscriberList) {
            if (subscriberRepository.findByMsisdn(dumpSubscriber.getMsisdn()) == null) {
                SubscriberCassandra subscriberCassandra = new SubscriberCassandra();
                try {
                    subscriberCassandra = restTemplate.getForObject("http://172.25.208.31/provisioning/mercurio-service/sps/subscriber/" + dumpSubscriber.getMsisdn(), SubscriberCassandra.class);
                } catch (HttpClientErrorException e) {
                    subscriberCassandra = null;
                } catch (Exception e) {
                    logger.info("Error while searching number in cassandra! ", e);
                    subscriberCassandra = null;
                }

                if (subscriberCassandra != null) {
                    Subscriber subscriber = new Subscriber();
                    subscriber.setMsisdn(dumpSubscriber.getMsisdn());
                    subscriber.setCreationDate(subscriberCassandra.getCreationDate());
                    subscriber.setCreationUser(subscriberCassandra.getCreationUser());
                    subscriber.setExpireTime(subscriberCassandra.getExpireTime());
                    subscriber.setImsi(subscriberCassandra.getImsi());
                    subscriber.setMaxPerSecond(subscriberCassandra.getMaxPerSecond());
                    subscriber.setPrepaid(subscriberCassandra.isPrepaid());
                    subscriber.setPriority(subscriberCassandra.getPriority());
                    subscriber.setQueueReceiveLimit(subscriberCassandra.getQueueReceiveLimit());
                    subscriber.setQueueSendLimit(subscriberCassandra.getQueueSendLimit());

                    String type = subscriberCassandra.getType();
                    for (SubscriberType subscriberType : subscriberTypeList) {
                        if (subscriberType.getType().equals(type)) {
                            subscriber.setSubscriberType(subscriberType);
                        }
                    }

                    try {
                        subscriberRepository.save(subscriber);
                        addSubscriberHistoryEntry(subscriberCassandra.getCreationUser(), subscriberCassandra.getCreationDate(), LogAction.SAVE, subscriber);
                        logger.info("Subscriber saved {}", subscriber.getMsisdn());
                    } catch (Exception e) {
                        throw new Exception(e.getMessage());
                    }
                }
                dumpSubscriberRepository.save(new DumpSubscriber(dumpSubscriber.getId(), dumpSubscriber.getMsisdn(), true));
            } else {
                dumpSubscriberRepository.save(new DumpSubscriber(dumpSubscriber.getId(), dumpSubscriber.getMsisdn(), true));
                logger.info("Subscriber already exists: {}", dumpSubscriber.getMsisdn());
            }
        }
    }

    public void deleteAll() {
        subscriberRepository.deleteAll();
    }

    public void addSubscriberHistoryEntry(String user, Date date, LogAction action, Subscriber subscriber) {
        SubscriberHistory entry = new SubscriberHistory(subscriber.getMsisdn(), date, user, action.toString(),
                parseObject(subscriber));
        subscriberHistoryRepository.save(entry);
    }

    private String parseObject(Object obj) {
        try {
            return mapper.writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            logger.error("Error parsing object for logging ->", e);
            return "{\"error\":\"Not able to parse object\"}";
        }
    }
}
