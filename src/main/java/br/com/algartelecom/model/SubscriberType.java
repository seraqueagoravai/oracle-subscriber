package br.com.algartelecom.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "subscriber_type", catalog = "smsadm")
public class SubscriberType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "subscriber_type_sq")
    @SequenceGenerator(name = "subscriber_type_sq", allocationSize = 1, sequenceName = "subscriber_type_sq", schema = "smsadm")
    private Long id;

    @Column(name = "type")
    private String type;

    @Column(name = "description")
    private String description;

    @Column(name = "prepaid")
    private Boolean prepaid;

    @Column(name = "max_per_second")
    private Integer maxPerSecond;

    @Column(name = "queue_send_limit")
    private Integer queueSendLimit;

    @Column(name = "queue_receive_limit")
    private Integer queueReceiveLimit;

    @Column(name = "imsi")
    private String imsi;

    @Column(name = "priority")
    private Integer priority;

    @Column(name = "creation_date")
    private Date creationDate;

    @Column(name = "creation_user")
    private String creationUser;

    @Column(name = "update_date")
    private Date updateDate;

    @Column(name = "update_user")
    private String updateUser;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getPrepaid() {
        return prepaid;
    }

    public void setPrepaid(Boolean prepaid) {
        this.prepaid = prepaid;
    }

    public Integer getMaxPerSecond() {
        return maxPerSecond;
    }

    public void setMaxPerSecond(Integer maxPerSecond) {
        this.maxPerSecond = maxPerSecond;
    }

    public Integer getQueueSendLimit() {
        return queueSendLimit;
    }

    public void setQueueSendLimit(Integer queueSendLimit) {
        this.queueSendLimit = queueSendLimit;
    }

    public Integer getQueueReceiveLimit() {
        return queueReceiveLimit;
    }

    public void setQueueReceiveLimit(Integer queueReceiveLimit) {
        this.queueReceiveLimit = queueReceiveLimit;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "SubscriberType{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", description='" + description + '\'' +
                ", prepaid=" + prepaid +
                ", maxPerSecond=" + maxPerSecond +
                ", queueSendLimit=" + queueSendLimit +
                ", queueReceiveLimit=" + queueReceiveLimit +
                ", imsi='" + imsi + '\'' +
                ", priority=" + priority +
                '}';
    }
}

