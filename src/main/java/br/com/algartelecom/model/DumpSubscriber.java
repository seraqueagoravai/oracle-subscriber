package br.com.algartelecom.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "dump_subscriber", catalog = "smsadm")
public class DumpSubscriber implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "dump_subscriber_sq")
    @SequenceGenerator(name = "dump_subscriber_sq", allocationSize = 1, sequenceName = "dump_subscriber_sq", schema = "smsadm")
    private Long id;

    @Column(name = "msisdn")
    private String msisdn;

    @Column(name = "flag")
    private Boolean flag;

    public DumpSubscriber() {
    }

    public DumpSubscriber(Long id, String msisdn, Boolean flag) {
        this.id = id;
        this.msisdn = msisdn;
        this.flag = flag;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public Boolean getFlag() {
        return flag;
    }

    public void setFlag(Boolean flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "DumpSubscriber{" +
                "id=" + id +
                ", msisdn='" + msisdn + '\'' +
                ", flag='" + flag + '\'' +
                '}';
    }
}
