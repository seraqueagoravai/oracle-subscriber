package br.com.algartelecom.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "subscriber", catalog = "smsadm")
public class Subscriber implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "subscriber_sq")
    @SequenceGenerator(name = "subscriber_sq", allocationSize = 1, sequenceName = "subscriber_sq")
    private Long id;

    @Column(name = "msisdn")
    private String msisdn;

    @Column(name = "prepaid")
    private Boolean prepaid;

    @Column(name = "max_per_second")
    private Integer maxPerSecond;

    @Column(name = "queue_send_limit")
    private Integer queueSendLimit;

    @Column(name = "queue_receive_limit")
    private Integer queueReceiveLimit;

    @Column(name = "imsi")
    private String imsi;

    @Column(name = "priority")
    private Integer priority;

    @Column(name = "expire_time")
    private Long expireTime;

    @Column(name = "creation_date")
    private Date creationDate;

    @Column(name = "creation_user")
    private String creationUser;

    @Column(name = "update_date")
    private Date updateDate;

    @Column(name = "update_user")
    private String updateUser;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "subscriber_type_id")
    @JsonBackReference
    private SubscriberType subscriberType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public Boolean getPrepaid() {
        return prepaid;
    }

    public void setPrepaid(Boolean prepaid) {
        this.prepaid = prepaid;
    }

    public Integer getMaxPerSecond() {
        return maxPerSecond;
    }

    public void setMaxPerSecond(Integer maxPerSecond) {
        this.maxPerSecond = maxPerSecond;
    }

    public Integer getQueueSendLimit() {
        return queueSendLimit;
    }

    public void setQueueSendLimit(Integer queueSendLimit) {
        this.queueSendLimit = queueSendLimit;
    }

    public Integer getQueueReceiveLimit() {
        return queueReceiveLimit;
    }

    public void setQueueReceiveLimit(Integer queueReceiveLimit) {
        this.queueReceiveLimit = queueReceiveLimit;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Long getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Long expireTime) {
        this.expireTime = expireTime;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationUser() {
        return creationUser;
    }

    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public SubscriberType getSubscriberType() {
        return subscriberType;
    }

    public void setSubscriberType(SubscriberType subscriberType) {
        this.subscriberType = subscriberType;
    }

    @Override
    public String toString() {
        return "Subscriber{" +
                "id=" + id +
                ", msisdn='" + msisdn + '\'' +
                ", prepaid=" + prepaid +
                ", maxPerSecond=" + maxPerSecond +
                ", queueSendLimit=" + queueSendLimit +
                ", queueReceiveLimit=" + queueReceiveLimit +
                ", imsi='" + imsi + '\'' +
                ", priority=" + priority +
                ", expireTime=" + expireTime +
                ", creationDate=" + creationDate +
                ", creationUser='" + creationUser + '\'' +
                ", updateDate=" + updateDate +
                ", updateUser='" + updateUser + '\'' +
                ", subscriberType='" + subscriberType + '\'' +
                '}';
    }
}
