package br.com.algartelecom.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "subscriber_history", catalog = "smsadm")
public class SubscriberHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "subscriber_history_sq")
    @SequenceGenerator(name = "subscriber_history_sq", sequenceName = "subscriber_history_sq", allocationSize = 1, schema = "smsadm")
    private Long id;

    @Column(name = "msisdn")
    private String msisdn;

    @Column(name = "date_time")
    private Date dateTime;

    @Column(name = "username")
    private String username;

    @Column(name = "action")
    private String action;

    @Column(name = "content")
    private String content;

    public SubscriberHistory() {
    }

    public SubscriberHistory(String msisdn, Date dateTime, String username, String action, String content) {
        this.msisdn = msisdn;
        this.dateTime = dateTime;
        this.username = username;
        this.action = action;
        this.content = content;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public Date getDateTime() {
        return dateTime;
    }

    public void setDateTime(Date dateTime) {
        this.dateTime = dateTime;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "SubscriberHistory{" +
                "id=" + id +
                ", msisdn='" + msisdn + '\'' +
                ", dateTime=" + dateTime +
                ", username='" + username + '\'' +
                ", action='" + action + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
