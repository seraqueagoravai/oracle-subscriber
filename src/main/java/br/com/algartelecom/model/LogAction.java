package br.com.algartelecom.model;

public enum LogAction {
    SAVE,
    DELETE,
    UPDATE;

    private LogAction() {
    }
}
