package br.com.algartelecom.model;

import java.util.Date;

public class SubscriberCassandra {

    private String msisdn;
    private String type;
    private boolean prepaid;
    private int maxPerSecond;
    private int queueSendLimit;
    private int queueReceiveLimit;
    private String imsi;
    private int priority;
    private Long expireTime;
    private Date creationDate;
    private String creationUser;

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isPrepaid() {
        return prepaid;
    }

    public void setPrepaid(boolean prepaid) {
        this.prepaid = prepaid;
    }

    public int getMaxPerSecond() {
        return maxPerSecond;
    }

    public void setMaxPerSecond(int maxPerSecond) {
        this.maxPerSecond = maxPerSecond;
    }

    public int getQueueSendLimit() {
        return queueSendLimit;
    }

    public void setQueueSendLimit(int queueSendLimit) {
        this.queueSendLimit = queueSendLimit;
    }

    public int getQueueReceiveLimit() {
        return queueReceiveLimit;
    }

    public void setQueueReceiveLimit(int queueReceiveLimit) {
        this.queueReceiveLimit = queueReceiveLimit;
    }

    public String getImsi() {
        return imsi;
    }

    public void setImsi(String imsi) {
        this.imsi = imsi;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public Long getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(Long expireTime) {
        this.expireTime = expireTime;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationUser() {
        return creationUser;
    }

    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    @Override
    public String toString() {
        return "SubscriberCassandra{" +
                "msisdn='" + msisdn + '\'' +
                ", type='" + type + '\'' +
                ", prepaid=" + prepaid +
                ", maxPerSecond=" + maxPerSecond +
                ", queueSendLimit=" + queueSendLimit +
                ", queueReceiveLimit=" + queueReceiveLimit +
                ", imsi='" + imsi + '\'' +
                ", priority=" + priority +
                ", expireTime=" + expireTime +
                ", creationDate=" + creationDate +
                ", creationUser='" + creationUser + '\'' +
                '}';
    }
}