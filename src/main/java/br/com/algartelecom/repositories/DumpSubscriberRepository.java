package br.com.algartelecom.repositories;

import br.com.algartelecom.model.DumpSubscriber;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DumpSubscriberRepository extends JpaRepository<DumpSubscriber, Long> {

    @Query(value = "SELECT * FROM DUMP_SUBSCRIBER WHERE FLAG = '0' FETCH NEXT 10 ROWS ONLY", nativeQuery = true)
    List<DumpSubscriber> getMsisdns();

    DumpSubscriber findByMsisdn(String msisdn);
}
