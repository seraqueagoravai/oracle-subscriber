package br.com.algartelecom.repositories;

import br.com.algartelecom.model.SubscriberType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubscriberTypeRepository extends JpaRepository<SubscriberType, Long> {

    SubscriberType findByType(String type);
}
