package br.com.algartelecom.repositories;

import br.com.algartelecom.model.SubscriberHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubscriberHistoryRepository extends JpaRepository<SubscriberHistory, Long> {
}
