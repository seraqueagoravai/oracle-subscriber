package br.com.algartelecom.repositories;

import br.com.algartelecom.model.Subscriber;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SubscriberRepository extends JpaRepository<Subscriber, Long> {

    Subscriber findByMsisdn(String msisdn);

    @Query(value = "SELECT * FROM SMSADM.SUBSCRIBER S WHERE NOT EXISTS (SELECT 1 FROM SMSADM.SUBSCRIBER_HISTORY H WHERE H.MSISDN = S.MSISDN)", nativeQuery = true)
    List<Subscriber> getSubscriberWithoutHistory();
}
