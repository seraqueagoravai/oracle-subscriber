package br.com.algartelecom.controller;

import br.com.algartelecom.service.SubscriberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/internalize")
public class InternalizeController {

    @Autowired
    SubscriberService subscriberService;

    @PostMapping("subscriber")
    public ResponseEntity saveSubscriber() throws Exception {
        subscriberService.saveSubscriber();
        return ResponseEntity.ok("Registros inseridos com sucesso!");
    }

    @DeleteMapping("subscriber")
    public ResponseEntity delete(){
        subscriberService.deleteAll();
        return ResponseEntity.ok().body("Registros removidos");
    }
}
