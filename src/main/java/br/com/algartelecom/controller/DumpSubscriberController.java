package br.com.algartelecom.controller;

import br.com.algartelecom.model.DumpSubscriber;
import br.com.algartelecom.service.DumpSubscriberService;
import br.com.algartelecom.service.MilhasSubscriberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/dump")
public class DumpSubscriberController {

    @Autowired
    DumpSubscriberService dumpSubscriberService;

    @Autowired
    MilhasSubscriberService milhasSubscriberService;

    @PostMapping()
    public ResponseEntity saveDump() {
        dumpSubscriberService.saveDump();
        return ResponseEntity.ok("Msisdns saved in oracle!");
    }

    @PostMapping(value = "/save")
    public ResponseEntity saveMilhas() {
        milhasSubscriberService.saveMilhas();
        return ResponseEntity.ok("Msisdns saved in oracle!");
    }

    @GetMapping()
    public List<DumpSubscriber> getAll() {
        return dumpSubscriberService.getAll();
    }

    @GetMapping(value = "/msisdns")
    public List<DumpSubscriber> getMsisdns() {
        return dumpSubscriberService.getMsisdns();
    }

    @PostMapping(value = "/flag/{msisdn}")
    public ResponseEntity<DumpSubscriber> changeFlagToOne(@PathVariable String msisdn) {
        return dumpSubscriberService.changeFlagToOne(msisdn);
    }

    @DeleteMapping()
    public void delete() {
        dumpSubscriberService.deleteMsisdns();
    }
}
