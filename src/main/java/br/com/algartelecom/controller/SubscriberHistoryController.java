package br.com.algartelecom.controller;

import br.com.algartelecom.service.SubscriberHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/history")
public class SubscriberHistoryController {

    @Autowired
    SubscriberHistoryService subscriberHistoryService;

    @PostMapping()
    public ResponseEntity createHistorys() {
        subscriberHistoryService.createHistorys();
        return ResponseEntity.ok("Historys created");
    }
}
